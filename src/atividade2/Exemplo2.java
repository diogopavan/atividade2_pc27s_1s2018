/**
 * Exemplo1: Programacao com threads
 * Autor: Diogo Reis Pavan
 * Ultima modificacao: 28/03/2018 23:40
 */
package atividade2;

/**
 *
 * @author Diogo Reis Pavan
 */
public class Exemplo2 {

    public static void main(String[] args) {

        System.out.println("Inicio da criacao das threads.");

        //Cria 20 THREADS
        for (int id = 1; id <= 20; id++) {
            Thread t = new Thread(new PrintTasks("thread"+id, id));
            //Inicia as threads, e as coloca no estado EXECUTAVEL
            //Thread no estado de pronto
            t.start();
        }

        System.out.println("Threads criadas");
    }

}
