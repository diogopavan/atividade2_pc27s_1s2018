/**
 * Exemplo1: Programacao com threads
 * Autor: Diogo Reis Pavan
 * Ultima modificacao: 28/03/2018 23:42
 */
package atividade2;

import java.util.Random;

/**
 *
 * @author Diogo Reis Pavan
 */
public class PrintTasks implements Runnable {

    private final int sleepTime; //tempo de adormecimento aleatorio para a thread
    private final String taskName; //nome da tarefa
    private final static Random generator = new Random();
    private int numInteiroSeq;
    private final int id;
    
    public PrintTasks(String name, int id){
        this.id = id;
        numInteiroSeq = 0;
        taskName = name;
        //Tempo aleatorio entre 0 e 1 segundos
        sleepTime = generator.nextInt(1000); //milissegundos
    }
    
    public void run(){
        
        try{
            if(this.id % 2 == 1){
                System.out.printf("Tarefa: %s dorme por %d ms\n", taskName, sleepTime);
                //Estado de ESPERA SINCRONIZADA
                //Nesse ponto, a thread perde o processador, e permite que
                //outra thread execute
                Thread.sleep(sleepTime);
            } else {
                System.out.printf("Numero sequencial: %d\n", numInteiroSeq);
                numInteiroSeq++;
                Thread.yield();
            }

        } catch (InterruptedException ex){
            System.out.printf("%s %s\n", taskName, "terminou de maneira inesperada.");
        }
        
       System.out.printf("%s acordou!\n", taskName);
    }
       
}
